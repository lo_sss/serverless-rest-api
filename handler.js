const AWS = require("aws-sdk");
const {
  checkCreateUserParams,
  checkUpdateUserParams,
  buildResponse,
  userExistsCheck,
} = require("./service");
const dynamodb = new AWS.DynamoDB.DocumentClient();

const dynamodbTableName = "Users";

const getUsers = async () => {
  const params = {
    TableName: dynamodbTableName,
  };
  const allUsers = await scanDynamoRecords(params, []);
  const body = {
    users: allUsers,
  };
  return buildResponse(200, body);
};

const getUser = async (request) => {
  const { id } = request.pathParameters;

  const params = {
    TableName: dynamodbTableName,
    Key: {
      id: +id,
    },
  };

  return await dynamodb
    .get(params)
    .promise()
    .then(
      (response) => {
        return buildResponse(200, response.Item);
      },
      (error) => {
        return buildResponse(
          400,
          `Can not get user with id: ${id} reason: ${error}`
        );
      }
    );
};

const scanDynamoRecords = async (scanParams, itemArray) => {
  try {
    const dynamoData = await dynamodb.scan(scanParams).promise();
    itemArray = itemArray.concat(dynamoData.Items);
    if (dynamoData.LastEvaluatedKey) {
      scanParams.ExclusiveStartkey = dynamoData.LastEvaluatedKey;
      return await scanDynamoRecords(scanParams, itemArray);
    }
    return itemArray;
  } catch (error) {
    console.error("Can not scan users", error);
  }
};

const createUser = async (requestBody) => {
  const parsedBody = await JSON.parse(requestBody.body);

  try {
    const getUsersResponse = await getUsers();
    const { users } = await JSON.parse(getUsersResponse.body);
    const { id } = parsedBody
    userExistsCheck(id, users, true);
    checkCreateUserParams(parsedBody);
  } catch (error) {
    return buildResponse(400, error.message);
  }

  const params = {
    TableName: dynamodbTableName,
    Item: parsedBody,
  };

  return await dynamodb
    .put(params)
    .promise()
    .then(
      () => {
        const body = {
          Operation: "SAVE",
          Message: "SUCCESS",
          Item: parsedBody,
        };
        return buildResponse(200, body);
      },
      (error) => {
        return buildResponse(400, `Can not save user! reason: ${error}`);
      }
    );
};

const updateUser = async (request) => {
  const parsedBody = await JSON.parse(request.body);
  const { id } = request.pathParameters;

  try {
    const getUsersResponse = await getUsers();
    const { users } = await JSON.parse(getUsersResponse.body);
    userExistsCheck(id, users);
    checkUpdateUserParams(parsedBody);
  } catch (error) {
    return buildResponse(400, error.message);
  }

  const { updateKey, updateValue } = parsedBody;
  const params = {
    TableName: dynamodbTableName,
    Key: {
      id: +id,
    },
    UpdateExpression: `set ${updateKey} = :value`,
    ExpressionAttributeValues: {
      ":value": updateValue,
    },
    ReturnValues: "UPDATED_NEW",
  };

  return await dynamodb
    .update(params)
    .promise()
    .then(
      (response) => {
        const body = {
          Operation: "UPDATE",
          Message: "SUCCESS",
          UpdatedAttributes: response,
        };
        return buildResponse(200, body);
      },
      (error) => {
        return buildResponse(
          400,
          `Can not update user with id: ${id} reason: ${error}`
        );
      }
    );
};

const deleteUser = async (request) => {
  const { id } = request.pathParameters;

  try {
    const getUsersResponse = await getUsers();
    const { users } = await JSON.parse(getUsersResponse.body);
    userExistsCheck(id, users);
  } catch (error) {
    return buildResponse(400, error.message);
  }

  const params = {
    TableName: dynamodbTableName,
    Key: {
      id: +id,
    },
    ReturnValues: "ALL_OLD",
  };
  return await dynamodb
    .delete(params)
    .promise()
    .then(
      (response) => {
        const body = {
          Operation: "DELETE",
          Message: "SUCCESS",
          Item: response,
        };
        return buildResponse(200, body);
      },
      (error) => {
        return buildResponse(
          400,
          `Can not delete user with id: ${id} reason: ${error}`
        );
      }
    );
};

module.exports = {
  getUser,
  getUsers,
  createUser,
  updateUser,
  deleteUser
}

