const { userParamsEnum } = require('./models');

const userExistsCheck = (id, allUsers, createMode) => {
    const foundUser = allUsers.find(user => user.id === +id)
    if(createMode && foundUser) throw new Error('Id Already Exists!')
    if(!createMode && !foundUser) throw new Error('Incorrect Id')
}

const checkCreateUserParams = (createUserParams) => {
  if (!createUserParams.id || !createUserParams.username || !createUserParams.email) {
    throw new Error(`Missing user parameters such as: id, username, email`);
  }
  if (createUserParams.username.length < 3) {
    throw new Error(`Username must be atleast 3 characters!`);
  }
};

const checkUpdateUserParams = async (body) => {
  if(!body.updateKey || !body.updateValue) throw new Error('Must provide update key and update value!')

  const { updateKey, updateValue } = body

  if (
    updateKey !== userParamsEnum.username &&
    updateKey !== userParamsEnum.email &&
    updateKey !== userParamsEnum.name &&
    updateKey !== userParamsEnum.address
  ) {
    throw new Error(`Not a valid key to update: ${parsedBody.updateKey}!`);
  }
  
  if (
    updateKey === userParamsEnum.username &&
    updateValue.length < 3
  ) {
    throw new Error(`Username must be atleast 3 characters!`);
  }

  if (
    updateKey === userParamsEnum.email &&
    !updateKey.includes('@')
  ) {
    throw new Error(
      `Not a valid email adress!`
    );
  }
};

const buildResponse = async (statusCode, body) => {
  return {
    statusCode: statusCode,
    headers: {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
    body: JSON.stringify(body),
  };
};

module.exports = {
  userExistsCheck,
  checkCreateUserParams,
  checkUpdateUserParams,
  buildResponse
}

