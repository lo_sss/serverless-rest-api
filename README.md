# User serverless REST API

Used Technologies: AWS, Node.js

# Features
* Create user
* Read user/users
* Update user
* Delete user

# Instalation
1. Clone the repo
2. npm install

# Sample calls:
* GET USERS

    URL: https://esydgugci8.execute-api.us-east-2.amazonaws.com/users/users
    body: /

* GET USER

    URL: https://kk37l9r2h6.execute-api.us-east-2.amazonaws.com/dev/user/2
    body: /

* PATCH USER

    URL: https://kk37l9r2h6.execute-api.us-east-2.amazonaws.com/dev/user/3
    body: {
    "updateKey": "email",
    "updateValue": "yasenia@yesenia.net"
    }

* DELETE USER

    URL: https://kk37l9r2h6.execute-api.us-east-2.amazonaws.com/dev/user/1
    body: /

* POST USER

    URL: https://kk37l9r2h6.execute-api.us-east-2.amazonaws.com/dev/user
    body: {
    "id": 20,
    "name": "Charlie Chaplin",
    "username": "chaplin",
    "email": "chaplin@gmail.com",
    "address": "wall of fame"
    }
